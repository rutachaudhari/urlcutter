package com.springapp.urlcutter.Service;

import com.springapp.urlcutter.Dto.ResponseDto;
import com.springapp.urlcutter.Model.UrlCutterClass;
import com.springapp.urlcutter.Repository.UrlCutterRepository;
import com.springapp.urlcutter.Util.ServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.RedirectView;

@Service
public class UrlCutterService {

    @Autowired
    private UrlCutterRepository urlCutterRepository;

    private UrlCutterClass urlCutterClass = new UrlCutterClass();

    //www.google.com
    public ResponseDto cutUrl(String longUrl, String shortUrl) {
        ResponseDto responseDto = new ResponseDto();
        urlCutterClass.setLongUrl(longUrl);
        try {
            urlCutterClass.setShortUrl(shortUrl);
            urlCutterRepository.save(urlCutterClass);
            responseDto.setShortUrl(shortUrl);
            responseDto.setMessage("ShortURL was created!");
            return responseDto;
        } catch (Exception e) {
            ServiceUtil serviceUtil = new ServiceUtil();
            String sUrl = serviceUtil.randomAlphaNumeric(4);
            urlCutterClass.setShortUrl(sUrl);

            urlCutterRepository.save(urlCutterClass);
            responseDto.setShortUrl(sUrl);
            responseDto.setMessage("The custom alias you've chosen is not available. We've created a random one for you instead ");
            return responseDto;
        }
    }

    public RedirectView getUrlbyShortUrl(String shortUrl) {
        RedirectView redirectView = new RedirectView();
        try {
            UrlCutterClass urlCutterClass = urlCutterRepository.fetchUrlByShortUrl(shortUrl);
            redirectView.setUrl("https://"+urlCutterClass.getLongUrl());
            return redirectView;
        } catch (Exception e) {
            redirectView.setStatusCode(HttpStatus.NOT_FOUND);
            redirectView.setUrl("");
            return redirectView;
        }
    }
}
