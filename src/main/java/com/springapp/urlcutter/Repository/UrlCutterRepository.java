package com.springapp.urlcutter.Repository;

import com.springapp.urlcutter.Model.UrlCutterClass;


public interface UrlCutterRepository {
    public UrlCutterClass fetchUrlByShortUrl(String sUrl);

    public void save(UrlCutterClass urlCutterClass);

}
