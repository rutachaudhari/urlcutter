package com.springapp.urlcutter.Repository;

import com.springapp.urlcutter.Model.UrlCutterClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class UrlCutterRepositoryImpl implements UrlCutterRepository {

    @Autowired
    private MongoOperations operations;

    public UrlCutterClass fetchUrlByShortUrl(String sUrl) {
        return operations.findOne(query(where("shortUrl").is(sUrl)), UrlCutterClass.class);
    }

    @Override
    public void save(UrlCutterClass urlCutterClass) {
        operations.save(urlCutterClass);
    }


}
