package com.springapp.urlcutter.Controller;


import com.springapp.urlcutter.Dto.ResponseDto;
import com.springapp.urlcutter.Model.UrlCutterClass;
import com.springapp.urlcutter.Service.UrlCutterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;


@RestController
@RequestMapping("/urlcutter")
@Slf4j
public class UrlCutterController {

    @Autowired
    private UrlCutterService urlCutterService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getForm() {
        return "login";
    }

    @RequestMapping(value = "/cuturl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity cutUrl(@RequestBody UrlCutterClass urlCutterClass) {
        ResponseDto responseDto = urlCutterService.cutUrl(urlCutterClass.getLongUrl(), urlCutterClass.getShortUrl());
        return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/{surl}", method = RequestMethod.GET)
    public RedirectView getUrl(@PathVariable(value = "surl") String sUrl) {
        return urlCutterService.getUrlbyShortUrl(sUrl);
    }

}
