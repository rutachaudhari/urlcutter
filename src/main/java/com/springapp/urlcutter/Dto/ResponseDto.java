package com.springapp.urlcutter.Dto;

public class ResponseDto {

    private String shortUrl;
    private String message;

    public ResponseDto(String shortUrl, String message) {
        this.shortUrl = shortUrl;
        this.message = message;
    }

    public ResponseDto() {
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
